import dataConversion from "./modules/searchResults.js"
import express from "express"
import {join} from "path"

// auto load env files
import 'dotenv/config'

// get current directory
const __dirname = process.cwd();

const app = express()

const PORT = process.env.PORT || 8000;

app.use(express.json())

app.get("/",(req,res)=>{
    const htmlFilePath = join(__dirname,`index.html`);
    res.sendFile(htmlFilePath);
})


app.get("/search/q=:searchQuery",async (req,res)=>{
    const {searchQuery} = req.params;
    const {api_key} = req.query;

    try {
        const response = await dataConversion(api_key,searchQuery)
        res.send(response)
    } 
    catch (error) {
        res.json(error)
    }
})

app.listen(PORT, ()=> console.log("port is running on localhost:", PORT))
