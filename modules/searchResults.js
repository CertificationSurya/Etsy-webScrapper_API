import cheerio from 'cheerio'

// fetch Data
const fetchData = async(api_key,searchQuery) =>{
    console.log("fetching...")
    let response = await fetch(`http://api.scraperapi.com?api_key=${api_key}&url=https://www.etsy.com/search?q=${searchQuery}&ref=search_bar`)
    response = await response.text()
    console.log(response)
    return response;
}

// sends data
export default async function dataTransport(api_key,searchQuery){

const data = await fetchData(api_key,searchQuery);
// load fetched web data to cheerio
const $ = cheerio.load(data);
const dataList = [];
conversionAndStoring($,dataList)

console.log("fetched");
return dataList;
}

// extract required data form Html data response to an array
const conversionAndStoring =($, dataList)=>{
    
// extraction using cheerio 
$('.js-merch-stash-check-listing').each((index, element) => {
   
    let title = $(element).find('h3').text();
    title = title.trim()
  
    const image = $(element).find('[data-listing-card-listing-image]').attr('src');
  
    let price = $(element).find('.currency-value').text()
    price = `$${parseFloat(price).toFixed(2)}`
  
    const boughtBy = $(element).find('.wt-text-gray').text().trim();
    const rating = $(element).find('.black-stars').attr("aria-label");
  
    dataList.push({title,image,price,boughtBy,rating});
  });

}